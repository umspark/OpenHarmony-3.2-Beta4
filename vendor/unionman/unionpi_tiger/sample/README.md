## 样例导航

基础样例列表如下

|  No  | 样例名称                  | 路径                                                         |
| :--: | ------------------------- | ------------------------------------------------------------ |
|  1   | GPIO控制                  | [unionpi_tiger/sample/hardware/gpio](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/hardware/gpio) |
|  2   | PWM舵机调试               | [unionpi_tiger/sample/hardware/pwm](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/hardware/pwm) |
|  3   | ADC设备读取采样数据       | [unionpi_tiger/sample/hardware/adc](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/hardware/adc) |
|  4   | I2C读写及温湿度传感器使用 | [unionpi_tiger/sample/hardware/i2c](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/hardware/i2c) |
|  5   | SPI读写                   | [unionpi_tiger/sample/hardware/spi](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/hardware/spi) |

进阶样例列表如下

|  No  | 样例名称                         | 路径                                                         |
| :--: | :------------------------------- | ------------------------------------------------------------ |
|  1   | HAP应用——控制GPIO                | [unionpi_tiger/sample/app/gpioled_app](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/app/gpioled_app) |
|  2   | HAP应用——控制舵机                | [unionpi_tiger/sample/app/pwm_app](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/app/pwm_app) |
|  3   | HAP应用——读取ADC采样数据         | [unionpi_tiger/sample/app/adc_app](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/app/adc_app) |
|  4   | HAP应用——I2C读取温湿度传感器数据 | [unionpi_tiger/sample/app/i2c_app](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/app/i2c_app) |
|  5   | HAP应用——SPI读写                 | [unionpi_tiger/sample/app/spi_app](https://gitee.com/openharmony/vendor_unionman/tree/master/unionpi_tiger/sample/app/spi_app) |
