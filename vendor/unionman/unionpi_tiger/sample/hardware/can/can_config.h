/*
* Copyright (c) 2022 Unionman Technology Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
 
#ifndef CAN_CONFIG_H
#define CAN_CONFIG_H

#define UM_MASK_OF_EFF CAN_EFF_MASK

#define UM_FLAG_OF_EFF CAN_EFF_FLAG

#define UM_MASK_OF_SFF CAN_SFF_MASK

#define UM_FLAG_OF_RTR CAN_RTR_FLAG

#define VERSION "4.0.6"

#endif
